using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHealth : MonoBehaviour
{
    public GameManager myManager;

    public Rigidbody rb;

    public int playerHealthMax;
    public int playerHealth;

    public int slightlyDamage;
    public int moderateDamage;
    public int seriouslyDamage;

    public float velocityOfSlightlyDamage;
    public float velocityOfModerateDamage;
    public float velocityOfSeriouslyDamage;
    [Header("Text and Button")]
    public GameObject gameOverText; // game over text
    public GameObject restartButton;

    float sumOfVelocity;

    [Header("Text")]

    public Text playerHealthText;

    
    void Start()
    {
        playerHealth = playerHealthMax;
    }

    // Update is called once per frame
    void Update()
    {

        

        //sumOfVelocity = Mathf.Abs(rb.velocity.x) + Mathf.Abs(rb.velocity.y) + Mathf.Abs(rb.velocity.z);

        sumOfVelocity = rb.velocity.magnitude;

        playerHealthText.text = "HP:" + playerHealth; // show the player health text in this format: X



        if (playerHealth <= 0)
        {
            playerHealth = 0;
            gameOverText.SetActive(true);
            restartButton.SetActive(true);
        }


    }

    private void OnCollisionEnter(Collision collision)
    {
        float collisionVelocity = sumOfVelocity;
        if(collision.gameObject.tag == "ground") {
            collisionVelocity = Mathf.Abs(rb.velocity.y);
        }
        
        if (collisionVelocity > velocityOfSlightlyDamage && collisionVelocity <= velocityOfModerateDamage)
        {
            //slightly damage
            playerHealth -= slightlyDamage;


        }else if(collisionVelocity > velocityOfModerateDamage && collisionVelocity <= velocityOfSeriouslyDamage)
        {
            //Moderate damage
            playerHealth -= moderateDamage;

        }
        else if (collisionVelocity > velocityOfSeriouslyDamage)
        {
            //Seriously damage
            playerHealth -= seriouslyDamage;

        }
        

    }


    public void Reset()
    {
        playerHealth = playerHealthMax;
    }


}
