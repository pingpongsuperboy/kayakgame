using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class Timer : MonoBehaviour
{
    public GameManager myManager;

    public Text timertext;
    public int minute;
    public float timeLimit;
    public float timer = 0f;

    public bool isPaused;
    [Header("Text and Button")]
    public GameObject gameOverText; // game over text
    public GameObject restartButton;


    void Start()
    {
        timer = timeLimit;
    }

    public void PauseTimer()
    {
        isPaused = true;
    }

    public void UnpauseTimer()
    {
        isPaused = false;
    }

    public void Restart()
    {
        timer = timeLimit;
    }

    void Update()
    {
        if(!isPaused) {
            if (timer <= 0)
            {
                // myManager.lose = true;
                gameOverText.SetActive(true);
                restartButton.SetActive(true);
            }
            else //if (!myManager.lose)  //if game isn't over
            {
                timer -= Time.deltaTime; // keep the timer running
            }

            int minutesLeft = (int)timer / 60;
            int secondsLeft = (int)timer % 60;
            string secondsZeroPadding = secondsLeft < 10 ? "0" : "";
            timertext.text = $"Time until Christmas: \n{minutesLeft}:{secondsZeroPadding}{secondsLeft}"; // show the timer text in this format: X
        }
        else {
            timertext.text = "";
        }
        


    // clock(won't be used on Thursday)
        // if (timer < 10)
        // {
        //     timertext.text = " Time�G 12/24 23:"+minute+":0" + (int)(timer);
        // }else if(timer >= 10 && timer < 60)
        // {
        //     timertext.text = " Time�G 12/24 23:" + minute + ":" + (int)(timer);
        // }else if (timer >= 60)
        // {
        //     minute++;
        //     timer = 0;
        // }

        

        
        //timertext.text = "Time:" + (int)(timer * 100f) / 100f; // show the timer text in this format: X.XX
    }
}
