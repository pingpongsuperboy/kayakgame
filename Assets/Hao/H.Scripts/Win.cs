using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(AudioSource))]
public class Win : MonoBehaviour
{
    AudioSource audioData;
    public AudioClip clip;

    public GameObject black;
    public GameObject selfie;

    // Start is called before the first frame update
    void Start()
    {
        audioData = GetComponent<AudioSource>();

    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter(Collider other)
    {
        
        if(other.gameObject.name == "Player(Test)")
        {
            audioData.PlayOneShot(clip, 1f);
            black.SetActive(true);
            Debug.Log("win");
            Invoke("ShowSelfie", 5f);

        }


    }



    void ShowSelfie()
    {
        selfie.SetActive(true);
    }

}
