using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationAfterReading : MonoBehaviour
{

    public GameObject envelope;
    public GameObject letters;
    public GameObject otherStuffs;

    public Animator envelopeAnim;
    public Animator letterAnim;
    public Animator otherAnim;


    public float time = 0;
    bool moved = false;

    [Header("For the letter")]
    public float percent;
    float mod = 1;

    public Vector3 startPos;
    public Vector3 endPos;



    [Header("For the envelope")]

    public Transform from;
    public Transform to;
    public float rotateSpeed = 0.1f;

    void Start()
    {
        time = 0;
        moved = false;
    }
    

    void Update()
    {
        percent += Time.deltaTime * mod;

        if (percent <= 1.05)
        {

            letters.transform.position = Vector3.MoveTowards(endPos, startPos, percent);
        }
        else
        {
            time += Time.deltaTime / rotateSpeed;
            envelope.transform.rotation = Quaternion.Lerp(to.rotation, from.rotation, time);
        }


               
        if (time>1)
        {

            otherAnim.enabled = true;
            envelopeAnim.enabled = true;
            letterAnim.enabled = true;
            Invoke("LetterDisappear", 0.5f);
        }

    }


    void LetterDisappear()
    {
        otherStuffs.SetActive(false);
        letters.SetActive(false);
        envelope.SetActive(false);

    }


    public void Reset()
    {
        percent = 0;
        time = 0;
        this.gameObject.SetActive(false);
    }



}
