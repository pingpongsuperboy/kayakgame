using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class changeSelfie : MonoBehaviour
{
    public Sprite[] selfie;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void SetPostcardImage( int playerAttempts)
    {
        if( playerAttempts <= 8)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[0];
        }else if (playerAttempts > 8 && playerAttempts <= 18)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[1];
        }
        else if (playerAttempts > 18 && playerAttempts <= 28)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[2];
        }
        else if (playerAttempts > 28 && playerAttempts <= 38)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[3];
        }
        else if (playerAttempts > 38 && playerAttempts <= 48)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[4];
        }
        else if (playerAttempts > 48 && playerAttempts <= 58)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[5];
        }
        else if (playerAttempts > 58 && playerAttempts <= 68)
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[6];
        }
        else if (playerAttempts > 68 )
        {
            var image = GetComponent<Image>();
            image.sprite = selfie[7];
        }



    }

}
