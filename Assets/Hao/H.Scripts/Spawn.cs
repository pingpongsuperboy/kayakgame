using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn : MonoBehaviour
{
    public GameObject prefab;  //Snow Ball
    public float spawnTime = 0.5f;
    public float minSpaenTime = 0.01f;



    public float xRangemin;
    public float xRangemax;
    public float zRangemin;
    public float zRangemax;

    public float height = 4f;
    float startSpawn;
    void Start()
    {
        Invoke("Spawner", spawnTime);  // wait 0.5 sec and start to spawn
        startSpawn = spawnTime;
    }

    void Update()
    {

    }

    void Spawner()
    {
        GameObject newCyl = Instantiate(prefab); // instantiate a new meteor
        newCyl.transform.position = new Vector3( // randomize its position, in the range of the ground
                                        Random.Range(xRangemin, xRangemax), // range
                                        height,
                                        Random.Range(zRangemin, zRangemax) //range
            );
        spawnTime -= 0.01f; // Shorten the time gap between each meteor

        if (spawnTime < minSpaenTime) // if the meteors are too dense
        {
            spawnTime = startSpawn; // reset

        }
        Invoke("Spawner", spawnTime); // wait a certain sec and then run this function again
    }


}
