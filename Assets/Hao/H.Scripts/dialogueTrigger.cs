using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class dialogueTrigger : MonoBehaviour
{
    public GameObject dialogueBox1;
    public GameObject letter;
    public GameObject dialogueBox2;
    public GameObject dialogueBox3;
    public GameObject dialogueBox4;

    public GameObject reindeer;
    public GameObject santa;
    public GameObject mainScene;
    public GameObject introSceneObj;
    public Timer mainSceneTimer;

    public Transform reindeerFallenTransform;
    public Transform santaConcernedTransform;
    public bool skipCutscene = false;

    bool readedDialogue1 = false;
    bool readedDialogue2 = false;
    bool readed = false;
    bool isReadingLetter = false;
    void Start()
    {
        isReadingLetter = false;
        RenderSettings.fog = true;
        if(skipCutscene) {
            LoadNextScene();
        }
    }

    // Update is called once per frame
    void Update()
    {

        if (dialogueBox1 != null && !dialogueBox1.activeSelf && readedDialogue1 == false)
        {
            // reindeer.transform.position = new Vector3(3.088196f, 0.4f, -6.7702f);
            // Quaternion reindeerDown = Quaternion.Euler(0.744f, -184.457f, -87.412f);
            reindeer.transform.position = reindeerFallenTransform.position;
            reindeer.transform.rotation = reindeerFallenTransform.rotation;
            Invoke("TriggerDialogueTwo", 1f);
        }

        if (dialogueBox2 != null && !dialogueBox2.activeSelf && readedDialogue1 == true && readedDialogue2 == false)
        {
            // santa.transform.position = new Vector3(-1.7506f, 0, -7.2388f);
            santa.transform.position = santaConcernedTransform.position;
            Quaternion santaTurn = Quaternion.Euler(0, 81.81f, 0);
            // santa.transform.rotation = santaTurn;
            santa.transform.rotation = santaConcernedTransform.rotation;
            Invoke("TriggerDialogueThree", 1f);
        }



        if(dialogueBox3 != null && !dialogueBox3.activeSelf && readed == false && readedDialogue2 == true )
        {
            letter.SetActive(true);

            if (isReadingLetter && Input.GetMouseButtonDown(0))
            {
                letter.SetActive(false);
                dialogueBox4.SetActive(true);
                readed = true;
            }

            isReadingLetter = true;
        }


        // load next scene
        if(!dialogueBox4.activeSelf && readed == true)
        {   
            LoadNextScene();
        }


    }

    void TriggerDialogueTwo()
    {
        readedDialogue1 = true;
        dialogueBox2.SetActive(true);
    }

    void TriggerDialogueThree()
    {
        readedDialogue2 = true;
        dialogueBox3.SetActive(true);
    }


    void LoadNextScene()
    {
        mainScene.SetActive(true);
        introSceneObj.SetActive(false);
        mainSceneTimer.Restart();
        mainSceneTimer.UnpauseTimer();
        RenderSettings.fog = false;
        // SceneManager.LoadScene("snowDemo - Copy");
    }

}
