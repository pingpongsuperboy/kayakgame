using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationManagerForIntroCutscene : MonoBehaviour
{

    public GameObject envelope;
    public GameObject letters;


    
    bool pressed = false;

    float time = 0;

    [Header("For the letter")]
    public float percent;
    float mod = 1.5f;

    public Transform startPos;
    public Transform endPos;
    public float moveUpAmount;
    public AnimationCurve moveCurve;

    

    [Header("For the envelope")]

    public Transform from;
    public Transform to;
    public float rotateSpeed = 0.1f;

    Vector3 letterStartPos;
    Vector3 letterEndPos;
    bool isPlayingAnim;

    void Start()
    {
        time = 0;
        pressed = false;
        isPlayingAnim = true;
        letterStartPos = letters.transform.position;
        letterEndPos = new Vector3(letterStartPos.x, letterStartPos.y + moveUpAmount, letterStartPos.z);
    }
    


    void Update()
    {
        time += Time.deltaTime / rotateSpeed ;

        if (isPlayingAnim)
        {

            if (time < 1)
            {

                envelope.transform.rotation = Quaternion.Lerp(from.rotation, to.rotation, time);

            }
            else
            {
                envelope.transform.rotation = to.rotation;
                percent += Time.deltaTime * mod;

                letters.transform.position = Vector3.Lerp(letterStartPos, letterEndPos, moveCurve.Evaluate(percent));
            }

            if (letters.transform.position == letterEndPos && pressed == false)
            {
                
            }
        }
    }


    public void FinishReading()
    {
        pressed = true;
        isPlayingAnim = false;
        // gameObject.SetActive(false);    

    }


    public void Reset()
    {
        time = 0;
        percent = 0;
        pressed = false;
        isPlayingAnim = true;
    }


    







}
