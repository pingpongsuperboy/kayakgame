using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public int loseTime = 0;

    public GameObject timeManager;
    public GameObject player;

    public bool lose = false;

    [Header("Letters")]
    public GameObject letter;
    public GameObject letter2022;
    public GameObject letter2023;
    public GameObject letter2024;


    [Header("Text and Button")]
    public GameObject gameOverText; // game over text
    public GameObject restartButton;
    public GameObject startButton;


    float playerOriginalXpos;
    float playerOriginalYpos;
    float playerOriginalZpos;
    void Start()
    {
        playerOriginalXpos = player.transform.position.x;
        playerOriginalYpos = player.transform.position.y;
        playerOriginalZpos = player.transform.position.z;


    }

    void Update()
    {
        if(lose == true)
        {
            gameOverText.SetActive(true);
            restartButton.SetActive(true);

        }
        
    }


    public void restartGame()
    {
        // loseTime++;
        // lose = false;

        // // reset timer and health
        // timeManager.GetComponent<Timer>().timer = timeManager.GetComponent<Timer>().timeLimit;
        // player.GetComponent<PlayerHealth>().playerHealth = player.GetComponent<PlayerHealth>().playerHealthMax;

        // //reset position
        // player.transform.position = new Vector3(playerOriginalXpos, playerOriginalYpos, playerOriginalZpos);


        // // show text and button
        // gameOverText.SetActive(false);
        // restartButton.SetActive(false);

        // startButton.SetActive(true);
        // letter.SetActive(true);
        // if(loseTime == 1)
        // {
        //     letter2022.SetActive(true);
        // }
        // if (loseTime == 2)
        // {
        //     letter2022.SetActive(false);
        //     letter2023.SetActive(true);
        // }
        // if (loseTime == 3)
        // {
        //     letter2023.SetActive(false);
        //     letter2024.SetActive(true);
        // }

    }

    public void startGame()
    {
        // letter.SetActive(false);
        // startButton.SetActive(false);

    }


}
