using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallExplode : MonoBehaviour
{
    public GameObject prefab; // smallball
    public float explodeTime = 0.02f;
    public float maxExplodeTime = 0f;
    void Start()
    {

    }

    void Update()
    {

    }

    void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.name == "ground")  // if collide with ground
        {
            Explode();
        }

    }

    void Explode()
    {

        GameObject smallBall = Instantiate(prefab); // instantiate a new smallball( meteor fragment)
        smallBall.transform.position = new Vector3( // randomize its position
                                    Random.Range(transform.position.x - 0.1f, transform.position.x + 0.1f),
                                    transform.position.y + 0.5f,
                                    Random.Range(transform.position.z - 0.1f, transform.position.z + 0.1f)
        );
        explodeTime -= 0.08f; // Shorten the time gap between each meteor fragment

        if (explodeTime < maxExplodeTime)
        {
            Destroy(gameObject); // destroy the meteor eventually
        }
        else
        {
            Invoke("Explode", explodeTime);  // wait a certain sec and then run this function again(make a new meteor fragment)
        }



    }
}
