using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class endingCutsceneManager : MonoBehaviour
{
    public Camera camera1;
    public Camera camera2;
    public Camera camera3;
    public Camera camera4;
    public Camera camera5;

    public float timeOfScene1;
    public float timeOfScene2;
    public float timeOfScene3;
    public float timeOfScene4;
    public float timeOfScene5;
    public float timeOfDisplayingText;

    public GameObject player1;
    public GameObject player2;
    public GameObject player3;
    public GameObject player4;
    public GameObject player5;

    public GameObject house4;
    public GameObject house5;

    public GameObject black;
    public GameObject black2;

    public GameObject text;

    float time;


    void Start()
    {
        time = 0;   
    }

    // Update is called once per frame
    void Update()
    {
        time += Time.deltaTime;


        if(time < timeOfScene1)
        {

            camera1.gameObject.SetActive(true);
            player1.SetActive(true);

        }
        else if ( time >= timeOfScene1 && time < timeOfScene2)
        {
            camera1.gameObject.SetActive(false);
            player1.SetActive(false);
            player2.SetActive(true);
            camera2.gameObject.SetActive(true);
        }
        else if (time >= timeOfScene2 && time < timeOfScene3)
        {
            camera2.gameObject.SetActive(false);
            player2.SetActive(false);
            player3.SetActive(true);
            camera3.gameObject.SetActive(true);
        }
        else if (time >= timeOfScene3 && time < timeOfScene4)
        {
            camera3.gameObject.SetActive(false);
            player3.SetActive(false);
            house4.SetActive(true);
            player4.SetActive(true);
            camera4.gameObject.SetActive(true);
        }
        else if (time >= timeOfScene4 && time < timeOfScene5)
        {
            camera4.gameObject.SetActive(false);
            player4.SetActive(false);
            house4.SetActive(false);
            player5.SetActive(true);
            house5.SetActive(true);
            camera5.gameObject.SetActive(true);
        }else if (time >= timeOfScene5 && time < timeOfDisplayingText )
        {
            
            black.SetActive(true);

        }else if (time >= timeOfDisplayingText)
        {
            text.SetActive(true);
            black2.SetActive(true);

        }


    }
}
