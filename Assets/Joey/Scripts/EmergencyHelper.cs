using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmergencyHelper : MonoBehaviour
{
    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Y)) {
            transform.position = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);
        }
    }
}
