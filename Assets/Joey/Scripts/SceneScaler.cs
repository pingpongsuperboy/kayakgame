using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneScaler : MonoBehaviour
{
    public RectTransform memoryScene;
    public RectTransform dreamScene;
    [Range(0, 0.5f)]
    public float sceneViewWidths = 1.0f;
    public float sceneViewHeightPadding = 20;


    void Update()
    {
        SetSceneViewTransforms();
    }

    
    void SetSceneViewTransforms()
    {
        float width = Screen.width * (1.0f - sceneViewWidths) * 0.5f;

        float left  = (Screen.width * sceneViewWidths) * 0.25f;
        float right = Screen.width - (Screen.width * sceneViewWidths) - left - (left * 0.5f);

        // set the Left value in memory rectTransform
        float leftVal = Screen.width * (1.0f - sceneViewWidths);
        memoryScene.offsetMin = new Vector2(leftVal, memoryScene.offsetMin.y);

        // set the Right value in dream rectTransform
        float rightVal = Screen.width * (1.0f - sceneViewWidths);
        dreamScene.offsetMax = new Vector2(-rightVal, dreamScene.offsetMax.y);

        SetLeft(memoryScene, left);
        SetLeft(dreamScene, right);
        SetRight(memoryScene, right);
        SetRight(dreamScene, left);

        // set top of both
        dreamScene.offsetMax  = new Vector2(dreamScene.offsetMax.x,  -sceneViewHeightPadding);
        memoryScene.offsetMax = new Vector2(memoryScene.offsetMax.x, -sceneViewHeightPadding);
        // set bottom of both
        dreamScene.offsetMin  = new Vector2(dreamScene.offsetMin.x, sceneViewHeightPadding);
        memoryScene.offsetMin = new Vector2(memoryScene.offsetMin.x, sceneViewHeightPadding);
    }


    void SetLeft(RectTransform rect, float left)
    {
        rect.offsetMin = new Vector2(left, rect.offsetMin.y);
    }

    void SetRight(RectTransform rect, float right)
    {
        rect.offsetMax = new Vector2(-right, rect.offsetMax.y);
    }

    void SetTop(RectTransform rect, float top)
    {
        rect.offsetMax  = new Vector2(rect.offsetMax.x,  -top);
    }

    void SetBottom(RectTransform rect, float bottom)
    {
        rect.offsetMin  = new Vector2(rect.offsetMin.x, bottom);
    }
}
