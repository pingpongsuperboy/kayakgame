using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerController_COPY : MonoBehaviour
{
    //declare rowing animator
    private Animator playerAnimator;

    //declare the clip name
    string clipName;

    //assign right controller button
    public KeyCode rightButton;
    //assign left controller button
    public KeyCode leftButton;

    //add paddle game objects
    public GameObject rightPaddle;
    public GameObject leftPaddle;

    //inverts which button does what
    public bool invertPaddleControls = false;

    // Start is called before the first frame update
    void Start()
    {
        //get the animator component and add it to the paddle animation
        playerAnimator = GetComponent<Animator>();

        if(invertPaddleControls) {
            var temp = rightButton;
            rightButton = leftButton;
            leftButton = temp;
        }

    }

    // Update is called once per frame
    void Update()
    {
        //get current clip
        var currentClip = this.playerAnimator.GetCurrentAnimatorClipInfo(0);
        //get clip name
        clipName = currentClip[0].clip.name;

        //if key code is pressed down to go right
        if (Input.GetKeyDown(rightButton))
        {
            //set paddle animation to true
            //turn off the left paddle componenet
            playerAnimator.SetBool("isGoingRight", true);
            leftPaddle.SetActive(false);

        } else if (Input.GetKeyUp(rightButton)) {
            //left arm animation is false and sets to idle
            playerAnimator.SetBool("isGoingRight", false);
            leftPaddle.SetActive(true);
        }

        //if key code is pressed down to go left
        if (Input.GetKeyDown(leftButton))
        {
            //left arm animation is true
            playerAnimator.SetBool("isGoingLeft", true);
            rightPaddle.SetActive(false);

        } else if (Input.GetKeyUp(leftButton)) {
            //left arm animation is false and sets to idle
            playerAnimator.SetBool("isGoingLeft", false);
            rightPaddle.SetActive(true);
        }

        //if going right turn off left paddle
        if (clipName == "GoRight")
        {
            leftPaddle.SetActive(false);
        } else {
            leftPaddle.SetActive(true);
        }
        //if going left turn of right paddle
        if (clipName == "GoLeft")
        {
            rightPaddle.SetActive(false);
        } else {
            rightPaddle.SetActive(true);
        }
    }
}
