using System.Collections;

using System.Collections.Generic;

using UnityEngine;

public class PadParticles : MonoBehaviour
{
    public ParticleSystem particle;

    public float isGroundedMaxDistance = 1;

    public LayerMask groundedMask;

    MoveOnPaddleTrigger movementScript;

    void Start()
    {
        movementScript = GetComponent<MoveOnPaddleTrigger>();
    }

    void OnTriggerEnter(Collider other)
    {
        if(!movementScript.IsGrounded())
            return;

        particle.Play();
    }

    
    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, Vector3.down * isGroundedMaxDistance);
    }

    bool IsGrounded()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, isGroundedMaxDistance, groundedMask)) {
            if(hit.transform.tag == "ground")
                return true;
        }

        return false;

    }

}