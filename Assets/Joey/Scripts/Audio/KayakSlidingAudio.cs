using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KayakSlidingAudio : MonoBehaviour
{
    [Header("Audio Sources")]
    public AudioSource slowAudioSource;
    public AudioSource fastAudioSource;

    [Header("Fade Variables")]
    public float fastSpeedThreshsold;
    public float slowFadeTime;
    public float fastFadeTime;
    public AnimationCurve slowFadeCurve;
    public AnimationCurve fastFadeCurve;

    [Header("Volume Controls")]
    public float slowVolume;
    public float fastVolume;

    [Header("Player Information")]
    public Rigidbody playerRb;
    public MoveOnPaddleTrigger movementScript;

    Transform playerTransform;
    float maxGroundedDistance;
    LayerMask groundedMask;

    void Start()
    {
        playerTransform = playerRb.GetComponent<Transform>();
        maxGroundedDistance = movementScript.isGroundedMaxDistance;
        groundedMask = movementScript.groundedMask;
    }


    void Update()
    {
        // if not moving at all OR you're not touching the ground, no sound
        float velocity = playerRb.velocity.magnitude;
        if(velocity < float.Epsilon || !IsPlayerGrounded()) {
            StopAudio();
        }
        // if we're slow enough, play slow audio
        else if(velocity < fastSpeedThreshsold) {
            PlaySlowAudio(velocity);
        }
        // otherwise, play fast audio
        else {
            PlayFastAudio(velocity);
        }
        // Debug.Log($"velocity: {velocity}");
    }


    bool IsPlayerGrounded()
    {
        RaycastHit hit;
        if(Physics.Raycast(playerTransform.position, Vector3.down, out hit, maxGroundedDistance, groundedMask)) {
            if(hit.transform.tag == "ground")
                return true;
        }
        

        return false;
    }


    void StopAudio()
    {
        slowAudioSource.Stop();
        fastAudioSource.Stop();
    }


    void PlaySlowAudio(float velocity)
    {
        float lerpValue = slowFadeCurve.Evaluate(velocity / slowFadeTime);
        PlayAudio(slowVolume, slowAudioSource, lerpValue);
        
        // make sure fast audio isn't playing
        // this can happen if the player jumps and then lands again
        // or something to that effect
        if(fastAudioSource.isPlaying) {
            fastAudioSource.Stop();
        }
    }


    void PlayFastAudio(float velocity)
    {
        // first, play fast audio
        float lerpValue = fastFadeCurve.Evaluate((velocity - fastSpeedThreshsold) / fastFadeTime);
        PlayAudio(fastVolume, fastAudioSource, lerpValue);

        // then, adjust slow audio's volume (so we fade between the clips)
        float slowFadeOutVolume = 1.0f - lerpValue;
        slowAudioSource.volume = Mathf.Lerp(0, slowVolume, slowFadeOutVolume);
    }


    void PlayAudio(float maxVolume, AudioSource audio, float lerpValue)
    {
        // first, check audio is playing
        if(!audio.isPlaying) {
            audio.Play();
        }

        // set volume according to how fast we're going
        float volume = Mathf.Lerp(0, maxVolume, lerpValue);
        audio.volume = volume;
    }
}
