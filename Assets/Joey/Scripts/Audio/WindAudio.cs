using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class WindAudio : MonoBehaviour
{
    [Header("Volume Controls")]
    [Range(0, 1)]
    public float maxVolume;
    [Range(0, 1)]
    public float minVolume;
    public float fadeTime;
    public AnimationCurve fadeCurve;

    [Header("Player Information")]
    public Rigidbody playerRb;
    public MoveOnPaddleTrigger movementScript;

    Transform playerTransform;
    float maxGroundedDistance;
    LayerMask groundedMask;
    AudioSource audio;

    void Start()
    {
        playerTransform = playerRb.GetComponent<Transform>();
        maxGroundedDistance = movementScript.isGroundedMaxDistance;
        groundedMask = movementScript.groundedMask;

        audio = GetComponent<AudioSource>();
    }


    void Update()
    {
        SetWindVolume();
    }


    void SetWindVolume()
    {
        float distToGround = GetDistanceFromGround();
        float volumeLerp = fadeCurve.Evaluate(distToGround / fadeTime);
        audio.volume = Mathf.Lerp(minVolume, maxVolume, volumeLerp);
    }


    float GetDistanceFromGround()
    {
        RaycastHit hit;
        if(Physics.Raycast(playerTransform.position, Vector3.down, out hit, float.MaxValue, groundedMask)) {
            if(hit.transform.tag == "ground") {
                // because we consider maxGroundedDistance to be essentially a 0 distance to the ground,
                // return the actual distance MINUS maxGroundedDistance
                float distance = Mathf.Clamp(hit.distance - maxGroundedDistance, 0, 1000); // only care about clamping the bottom end, so set max to arbitrary big number
                return hit.distance;
            }
                
        }
        
        // something has gone wrong
        Debug.LogError($"WindAudio script never found the ground!");
        return 0;
    }
}
