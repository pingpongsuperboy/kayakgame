using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
[RequireComponent(typeof(MoveOnPaddleTrigger))]
public class RowingAudio : MonoBehaviour
{
    public AudioClip[] rowingClips;

    AudioSource audio;
    MoveOnPaddleTrigger moveOnPaddleTrigger;

    void Start()
    {
        audio = GetComponent<AudioSource>();
        moveOnPaddleTrigger = GetComponent<MoveOnPaddleTrigger>();
    }

    
    void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "paddle" && moveOnPaddleTrigger.IsGrounded()) {
            PlayRowingAudio();
        }
    }


    void PlayRowingAudio()
    {
        // make sure audio isn't playing
        if(audio.isPlaying) {
            audio.Stop();
        }

        // get random clip
        AudioClip clip = rowingClips[Random.Range(0, rowingClips.Length)];
        audio.clip = clip;
        
        // play random clip
        audio.Play();
    }
}
