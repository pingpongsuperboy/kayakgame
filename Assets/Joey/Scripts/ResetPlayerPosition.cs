using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetPlayerPosition : MonoBehaviour
{
    public Transform playerTransform;
    Vector3 startingPosition;
    Quaternion startingRotation;

    Rigidbody playerRigidbody;


    void Start()
    {
        playerRigidbody = playerTransform.GetComponent<Rigidbody>();
        startingPosition = playerTransform.position;
        startingRotation = playerTransform.rotation;
    }


    public void ResetPosition()
    {
        Debug.Log("restting player position");
        // reset transform
        playerTransform.position = startingPosition;
        playerTransform.rotation = startingRotation;

        // reset rigidbody
        playerRigidbody.velocity = Vector3.zero;
        playerRigidbody.angularVelocity = Vector3.zero;
    }
}
