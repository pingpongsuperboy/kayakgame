using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveOnPaddleTrigger : MonoBehaviour
{
    public Rigidbody playerRigidbody;

    [Tooltip("The type of ForceMode the water applies to the player. These are just built-in Unity physics options")]
    public ForceMode physicsForceMode = ForceMode.Force;   

    [Tooltip("How much force to move the player on trigger")]
    public float moveForce;

    [Tooltip("Direction to apply the movement force. Will be normalized")]
    public Vector3 forceDirection;

    public float isGroundedMaxDistance = 1;
    public LayerMask groundedMask;

    Transform playerTransform;

    bool isOarInWater;

    void Start()
    {
        forceDirection.Normalize();
        playerTransform = playerRigidbody.GetComponent<Transform>();

        isOarInWater = false;
    }


    void FixedUpdate()
    {
        if(isOarInWater) {
            ApplyTorque();
        }
    }


    void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "paddle" && IsGrounded()) {
            ApplyForce();

            isOarInWater = true;
        }
    }

    void OnTriggerExit(Collider other) 
    {
        if(other.tag == "paddle") {
            isOarInWater = false;
        }
    }


    void ApplyForce()
    {
        // convert world space direction to local space
        Vector3 localForceDirection = playerTransform.InverseTransformDirection(forceDirection);
        playerRigidbody.AddRelativeForce(forceDirection * moveForce, physicsForceMode);
    }


    void ApplyTorque()
    {

    }


    public bool IsGrounded()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position + new Vector3 (0,3,0), Vector3.down, out hit, isGroundedMaxDistance, groundedMask)) {
            if(hit.transform.tag == "ground")
                return true;
        }
        

        return false;
    }


    public void SetPhaseTwoForce()
    {
        moveForce = 80000;
    }
}
