using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadNextScene : MonoBehaviour
{
    public string sceneName;
    public bool loadOnTimer;
    public float timer;


    public void Load()
    {
        Load(sceneName);
    }


    public void Load(string scene)
    {
        SceneManager.LoadScene(scene, LoadSceneMode.Single);
    }
}
