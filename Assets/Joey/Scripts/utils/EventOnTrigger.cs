using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class EventOnTrigger : MonoBehaviour
{
    public UnityEvent eventsOnTrigger;
    public string targetTag = "Player";


    void OnTriggerEnter(Collider other)
    {
        if(other.CompareTag(targetTag) || targetTag == "") {
            eventsOnTrigger.Invoke();
        }
    }
}
