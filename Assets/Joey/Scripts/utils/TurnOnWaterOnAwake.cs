using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnWaterOnAwake : MonoBehaviour
{
    public WaterSystem.Water water;

    void Awake()
    {
        water.enabled = true;
    }
}
