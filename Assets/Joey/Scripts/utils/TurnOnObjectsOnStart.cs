using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnOnObjectsOnStart : MonoBehaviour
{
    public GameObject[] objects;

    void Awake()
    {
        foreach(var obj in objects) {
            obj.SetActive(true);
        }
    }
}
