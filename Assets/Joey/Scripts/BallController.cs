using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallController : MonoBehaviour
{
    public float moveSpeed;
    public Camera followCam;

    Rigidbody rigidbody;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.W)) {
            rigidbody.AddForce(followCam.transform.forward * moveSpeed, ForceMode.Force);
        }
        if(Input.GetKey(KeyCode.A)) {
            rigidbody.AddForce(-followCam.transform.right * moveSpeed, ForceMode.Force);
        }
        if(Input.GetKey(KeyCode.S)) {
            rigidbody.AddForce(-followCam.transform.forward * moveSpeed, ForceMode.Force);
        }
        if(Input.GetKey(KeyCode.D)) {
            rigidbody.AddForce(followCam.transform.right * moveSpeed, ForceMode.Force);
        }
    }
}
