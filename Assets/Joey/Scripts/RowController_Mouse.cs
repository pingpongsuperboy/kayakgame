using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RowController_Mouse : MonoBehaviour
{
    [Header("Animation Settings")]
    public Animator animator;
    public AnimationClip clipToPlay;

    [Header("Mouse Settings")]
    [Range(0, 0.01f)]
    public float mouseSensitivity;
    
    // we'll always clamp this between [0,1]
    float currentAnimationFrame;

    void Start()
    {
        currentAnimationFrame = 0;
        HideAndLockMouse();
    }


    void Update()
    {
        float mouseDelta = Input.GetAxis("Mouse X") * mouseSensitivity;
        UpdateAnimation(mouseDelta);
    }


    void UpdateAnimation(float mouseDelta)
    {
        // update animation frame based on mouse delta. clamp value [0,1]
        currentAnimationFrame = Mathf.Clamp(currentAnimationFrame + mouseDelta, 0, 1);

        // map [0, 1] to [0, animationLength], and store
        float targetTime = clipToPlay.length * currentAnimationFrame;

        // update animation frame
        animator.Play(clipToPlay.name, 0, targetTime);
        // set speed to 0
        animator.speed = 0;

    }


    void HideAndLockMouse()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
