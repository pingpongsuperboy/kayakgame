using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class LetterManager : MonoBehaviour
{
    public TextAsset letterFile;
    string[] letters;
    public Camera mainCamera;
    public Camera lettersCamera;
    public bool showLetterOnAwake;
    public TMP_Text letterContentTextMesh;
    public TMP_Text letterDateTextMesh;
    public GameObject lettersObject;
    public GameObject player;

    public Timer timer;
    public string signOffWhiteSpace;

    public Material[] envelopeMaterials;
    public GameObject[] envelopeMaterialObjects;

    public Sprite[] graffitiSprites;
    public Color[] graffitiColors;
    public SpriteRenderer graffiti1;
    public SpriteRenderer graffiti2;

    public GameObject[] graffitiObjects;

    bool isShowingLetter;
    int currentLetter;
    int currentYear;
    int materialIndex;
    public int graffitiIndex;
    public int graffitiColIndex;

    void Start()
    {
        currentLetter = 0;
        currentYear = 2021;
        materialIndex = 0;
        graffitiIndex = 0;
        graffitiColIndex = 0;
        isShowingLetter = false;

        letters = letterFile.text.Split('\n');

        if(showLetterOnAwake) {
            ShowNextLetter();
        }
    }


    void Update()
    {
        // if we're showing the letter, wait for any key to be pressed
        if(isShowingLetter) {
            // use key up to avoid problems with the kanoe moving on key release when the game starts
            if(Input.anyKeyDown && !Input.GetKey(KeyCode.R)) {
                Debug.Log("got any key down");
                // disable letter camera, and show the main camera
                // lettersCamera.gameObject.SetActive(false);
                // mainCamera.gameObject.SetActive(true);
                Invoke("HideLetter", 0.5f);
                isShowingLetter = false;
            }
        }
    }


    public void ShowNextLetter()
    {
        if(currentLetter >= letters.Length)
            return;

        // reset text
        ResetText();
        lettersObject.SetActive(true);
        int randomDate = Random.Range(1, 24);

        SetEnvelopeMaterial();
        SetGraffitiMaterial();

        // compile the letter, and set the canvas's text to it
        // string letterText = $"Dear Santa,\n\n{letters[currentLetter]}\n\nKyle\n<i>{currentYear}</i>";
        string letterText = $"Dear Santa,\n\n{letters[currentLetter]}\n\n{signOffWhiteSpace}Kyle\n{signOffWhiteSpace}{currentYear}";
        letterContentTextMesh.text = letterText;
        // letterDateTextMesh.text    = $"12/{randomDate}/{currentYear}";

        // show the letter camera
        mainCamera.gameObject.SetActive(false);
        lettersCamera.gameObject.SetActive(true);
        player.SetActive(false);

        // increment year and letter, and set isShowingLetter to true so we watch for updates in Update
        currentLetter++;
        currentYear++;
        isShowingLetter = true;
    }


    void ResetText()
    {
        letterContentTextMesh.text = "";
        letterDateTextMesh.text = "";
    }


    void HideLetter()
    {
        if(!isShowingLetter) {
            lettersObject.SetActive(false);
            player.SetActive(true);
            ResetText();
            timer.UnpauseTimer();
            lettersCamera.gameObject.SetActive(false);
            mainCamera.gameObject.SetActive(true);
        }
    }


    void SetEnvelopeMaterial()
    {
        // get material
        Material mat = envelopeMaterials[materialIndex];

        // assign material to the envlope objects
        foreach(var env in envelopeMaterialObjects) {
            env.GetComponent<Renderer>().material = mat;
        }

        // increment material index
        materialIndex = (materialIndex == envelopeMaterials.Length - 1) ? 0 : materialIndex + 1;
    }


    void SetGraffitiMaterial()
    {
        // // get material
        // Sprite sprite = graffitiSprites[graffitiIndex];
        // Color col1 = graffitiColors[graffitiColIndex];
        // Color col2 = graffitiColors[graffitiColIndex + 1];

        // // assign material to the envlope objects
        // graffiti1.sprite = sprite;
        // graffiti1.color = col1;

        // graffiti2.sprite = sprite;
        // graffiti2.color = col2;

        // // increment material index
        // graffitiIndex = (graffitiIndex == graffitiSprites.Length - 1) ? 0 : graffitiIndex + 1;
        // graffitiColIndex = (graffitiColIndex == graffitiColors.Length - 2) ? 0 : graffitiColIndex + 2;

        foreach(var graffiti in graffitiObjects) {
            graffiti.SetActive(false);
        }

        graffitiObjects[graffitiIndex].SetActive(true);

        graffitiIndex = (graffitiIndex == graffitiSprites.Length - 1) ? 0 : graffitiIndex + 1;
    }


    public void SetWhiteSpace(string newWhiteSpace)
    {
        signOffWhiteSpace = newWhiteSpace;
    }

    public void SetFontSize(float fontSize)
    {
        letterContentTextMesh.fontSize = fontSize;
    }
}
