using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatchDreamPosition : MonoBehaviour
{
    public enum Axis { x, y, z };

    public Transform dreamBoat;
    public Axis axisToMatch;

    void Update()
    {
        switch(axisToMatch) {
            case Axis.x:
                transform.position = new Vector3(dreamBoat.position.x, transform.position.y, transform.position.z);
                break;
            case Axis.y:
                transform.position = new Vector3(transform.position.x, dreamBoat.position.y, transform.position.z);
                break;
            case Axis.z:
                transform.position = new Vector3(transform.position.x, transform.position.y, dreamBoat.position.z);
                break;
            default:
                Debug.LogError("No axis chosen!");
                break;
        }
    }
}
