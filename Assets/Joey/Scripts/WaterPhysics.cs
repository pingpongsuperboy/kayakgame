using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider))]
public class WaterPhysics : MonoBehaviour
{
    // Buouyancy
    [Header("Buoyancy")]

    [Tooltip("This is how much force the water adds to the player while they're in the water. Higher value means bouncier water.")]
    public float buoyancy = 1.0f;
    [Tooltip("To be more life-like, we add a random amount of force each frame rather than a constant amount. This value says how big the random range should be. For example, if the buoyancy is 5 and this value is 1, then we the range of random values applied will be [4.5, 5.5]")]
    public float randomBuoyancyDistribution = 1.0f;
    [Tooltip("Set this to true if the player starts in the water. Otherwise, leave false.")]
    public bool buoyOnAwake = false;
    [Tooltip("The type of ForceMode the water applies to the player. These are just built-in Unity physics options")]
    public ForceMode physicsForceMode = ForceMode.Force;
    [Tooltip("To keep us from throwing the player super high up, adding a max for how high we throw the player")]
    public float maxPlayerVelocity = 100.0f;

    // Torque
    [Header("Torque")]

    [Tooltip("The type of ForceMode the water applies to the player when rotating. These are just built-in Unity physics options")]
    public ForceMode torqueForceMode = ForceMode.Force;  
    
    [Tooltip("How much force to rotate the player while the oar is in the water")]
    public float torqueForce;
    
    // Player Rigidbody
    [Header("Player Rigidbody")]
    public Rigidbody playerRigidbody;
    public bool applyWaterDrag;

    public float waterDrag;
    public float displacementMultiplier;

    bool buoyPlayer;
    bool isOarInWater;

    // helper vars for torque
    float leftPaddleTorque;
    float rightPaddleTorque;

    void Start()
    {
        randomBuoyancyDistribution *= 0.5f;
        leftPaddleTorque  =  torqueForce;
        rightPaddleTorque = -torqueForce;
    }


    void FixedUpdate()
    {
        // if player is in water, add force to them
        if(buoyPlayer) {
            BuoyPlayer(playerRigidbody);
        }
        if(isOarInWater) {
            ApplyTorque();
        }

        if(applyWaterDrag)
            playerRigidbody.AddForce(displacementMultiplier * -playerRigidbody.velocity * waterDrag * Time.fixedDeltaTime, ForceMode.VelocityChange);
    }


    void OnTriggerEnter(Collider other)
    {
        // if player enters us, we want to buoy them
        if(other.CompareTag("dreamPlayer") || other.CompareTag("memoryPlayer")) {
            buoyPlayer = true;
        }
        // if oar enters us, we want to rotate player
        if(other.CompareTag("paddleLeft") || other.CompareTag("paddleRight")) {
            isOarInWater = true;
            Debug.Log($"got paddle with tag {other.tag} and object name: {other.gameObject.name}");
            torqueForce = other.CompareTag("paddleLeft") ? leftPaddleTorque : rightPaddleTorque;
        }
    }


    void OnTriggerExit(Collider other)
    {
        // if player exits us, we want to STOP buoying them
        if(other.CompareTag("dreamPlayer") || other.CompareTag("memoryPlayer")) {
            buoyPlayer = false;
        }
        // if oar exits us, we want to STOP rotating player
        if(other.CompareTag("dreamPlayer") || other.CompareTag("memoryPlayer")) {
            isOarInWater = false;
        }
    }


    void BuoyPlayer(Rigidbody rb)
    {
        // don't add force if the player is already moving at max speed
        if(rb.velocity.y < maxPlayerVelocity) {
            float forceAmount = Random.Range(buoyancy - randomBuoyancyDistribution, buoyancy + randomBuoyancyDistribution);
            rb.AddForce(Vector3.up * (forceAmount * Time.deltaTime), physicsForceMode);
        }
    }


    void ApplyTorque()
    {
        playerRigidbody.AddRelativeTorque(Vector3.up * torqueForce, torqueForceMode);
    }
}
