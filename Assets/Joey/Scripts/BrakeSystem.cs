using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrakeSystem : MonoBehaviour
{
    public float brakeForce;

    Rigidbody rb;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    void FixedUpdate()
    {
        if(Input.GetKey(KeyCode.Space)) {
            rb.AddForce(-transform.forward * brakeForce * Time.fixedDeltaTime);
        }
    }
}
