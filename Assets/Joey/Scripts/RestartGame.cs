using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class RestartGame : MonoBehaviour
{
    [System.Serializable]
    public class RestartEvent {
        public UnityEvent events;
        public int restartCountToTrigger;
        public bool alwaysTrigger = false;
    }

    public RestartEvent[] restartEvents;

    int restartCounter;


    void Start()
    {
        restartCounter = 0;
    }


    void Update()
    {
        if(Input.GetKeyDown(KeyCode.R)) {
            Restart();
        }
    }


    public void Restart()
    {
        Debug.Log("restarting");
        foreach(var restartEvent in restartEvents) {
            if(restartEvent.restartCountToTrigger == restartCounter || restartEvent.alwaysTrigger) {
                if(restartEvent.alwaysTrigger) {
                    Debug.Log("restarting bcause always");
                }
                if(restartEvent.restartCountToTrigger == restartCounter) {
                    Debug.Log($"restarting because the counter is {restartCounter}");
                }
                restartEvent.events.Invoke();
            }
        }

        Debug.Log($"restart counter: {restartCounter}");

        restartCounter++;
    }
}
