I want a Kayak.
I still want a Kayak. Thank you.
I really want a Kayak, it’s okay to use a second-hand one.
Am I really a bad kid?
I am in elementary school this year, I hope I can get a Kayak, I want to take it to school!
I was laughed at by my classmates last week. They said that Santa Claus does not exist. Is that true?
Sorry I said you don't exist last year, Santa! Hope you do not mind! 
I cleaned the chimney last week! Welcome!
I  figured my sock is not big enough, so I sewed an oversized sock this year.  It will surely fit a Kayak!
Have you ever been near my house? The whole mountain has Christmas decorations I collected. You can visit it when you send the Kayak to me.
Will you come this year? I learned how to make pudding. I will put it on the table. When you come, remember to finish eating before leaving.
I joined the Kayak club at school, but I did not participate in the practice, I told the coach that I was still waiting for my Kayak
Coach said the trick of Kayaking is balancing, but balance is pieced together from lots of imbalance. What do you think that means?
I will graduate next year. I hope I can participate in the competition next year.
I will go to university next year, but I will definitely be back during the Christmas holidays
I got a Kayak in the lottery this year, but I returned it because I was about to have one
Be patient! I tell this to myself this time every year
I saw an inspiring sentence in the book last month, it said "Look at the road, don’t look at the cliff."
If something is blocking your direction, don’t give up.
I had a terrible year.
Merry Christmas.
Merry Christmas
Now I realized that kayaking is a sport that constantly corrects its direction. Don’t you think it’s very beautiful?
My friend said that maybe you took my kayak to play. If you have a good time, it doesn’t matter if I don’t receive it.
If something is blocking your direction, it’s okay to run away.
If you are tired, remember to rest
I told my friends about what I wrote to you. They said they would come to my house to wait for you with me this Christmas. They are good friends.
Hey, I don’t expect your coming for a moment this year, because I don’t know if you really send me a Kayak, next year I can continue to write to you, I am very conflicted.
What do you think a person does not give up for?
I got married this year and the good news is that we decided to live here.
Hey, I suddenly feel sorry, these letters may annoy you. I don’t think I should ask you for something like this. I’m really sorry, it must be tiring.
How are you doing recently? I have recently started to think about one thing, I feel that I am about to make a very important decision in my life.
I bought a Kayak this year.
I seem to be used to writing to you. How are you doing? Since I have bought a Kayak myself, I don’t know if you will receive this letter.
The weird thing is that after I bought a Kayak, I haven't played it a few times. Am I not interested anymore? I have no idea.
I accidentally bought too many ingredients this year, there should be a lot of leftovers for the Christmas dinner, I don’t know why I’m telling you this.
I actually don't know where these letters were sent, but none of them were sent back. Did you really receive them?
Thank you for reading my letter all the time. (but maybe you don't) That means a lot to me.
It has become colder in winter recently. Does Santa feel cold? Take care.
I tried snow kayaking with my friend this year. It's really difficult. Have you ever played it?
Merry Christmas. I passed a kindergarten last week and saw children smiling happily. That is somehow your credit, isn't it?
On Christmas Eve this year, I will pretend to be Santa and go to town to give gifts, so I won’t be at home (but I will be back the next day)
My child wants a bicycle this Christmas. Do you remember the oversized socks I sewed when I was a kid? Finally came in handy today haha.
I heard that some people have seen an old man snow kayaking on the hillside in recent years. Is that you?
Maybe you really exist, but the children in the world are too bad for you.
Hey, it must be a tiring thing to hear everyone’s wishes. I feel so sorry.
I went to the mountain last month and found that the Christmas decorations when I was a child were still there. I was really touched.
Originally, I thought about walking along the mountain on Christmas Eve. But I don’t know why I have a strong instinct to stay at home. I should stay at home. Will anything happen?
Last month I saw a child learning to walk, staggering forward. Seeing her becoming more and more proficient, it was as if she was connecting to this physical world.
Did you know this is the 50th letter? Would it look silly to insist on doing the same thing for 50 years?
Merry Christmas, do you also have a long-awaited thing? Will anyone give you a gift at Christmas?
When I sent the letter this year, the letter was snatched by the little gangsters in the town. They read the letter and teased me, but I don’t care, they don’t know the meaning of Christmas.
I finally don’t need to wear a fake white beard for this year’s Christmas event.
I had a serious illness this year. The doctor asked me to engage in outdoor sports. I think Kayaking might help.
I met the gangsters who snatched my letter before. They asked me why I believe in Santa. Why not?
I saw a Christmas wishing card written by a child last week. He said he wanted a Kayak. I cried and gave him my Kayak.
I don’t know if I’m getting old and have auditory hallucinations. In the past few years, every Christmas I can feel you coming close to my house with a Kayak close. I even heard the sound of the Kayak sliding on the snow.
How lucky is this in this world to be able to believe in Santa at this age?
In the past few years, I dreamed of you falling into the valley on Christmas Eve. Then I woke up in shockness. I hope you are safe.
People need some expectations to live by, right? Will this be the meaning of Christmas?
I have a lot of things to say, but I don’t have much strength to write. It would be great if I could chat with you directly hahaha, let’s talk about it next year.
The condition of illness has not improved. But I know that writing this letter will make me better.
Is Santa really immortal? That must be tiring.
I am getting used to being an old man, but I feel like a child when I write to you. I think it's because you are older than me haha
My child saw me writing this letter. When being asked what am I doing, I subconsciously said that I was making a gift. Why would I say that?
I know why I say that writing these cards is making a gift. This is my Christmas present to myself.
I hope I can let you know how much I thank you, Santa.
I don't actually want a Kayak. I want those things that Kayak carries. Do you know what they are?
I have been experiencing a cycle of expectation and disappointment throughout my life. I always want someone to love me.
But in fact, what I want most is that I can love myself. That’s why I keep writing these cards every year.
I tried to show kindness to myself. But I keep failing. I can't like myself.
It may not be possible in my lifetime. But you know what? Even if you are far away from perfection. But the thought that you are willing to try to get close to it is perfect enough.
I know. I know all about it. You look at these in another dimension. I know you have been trying hard. You are really really really great.
My body is getting weaker and weaker. Writing a letter is becoming more and more difficult for me. This is probably my last letter to you. But it doesn't matter. Because I know you don't need to know my thoughts through letters anymore. You understand them all, right? Thank you Santa