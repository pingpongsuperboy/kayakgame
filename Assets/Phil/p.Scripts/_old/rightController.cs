using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rightController : MonoBehaviour
{
    private Animator paddleAnimator;
    private bool isGoingRight;

    //assign right controller button
    public KeyCode rightButton;

    // Start is called before the first frame update
    void Start()
    {
        //get the animator component and add it to the paddle animation
        paddleAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //if key code is pressed down
        if (Input.GetKeyDown(rightButton))
        {
            //left arm animation is true
            paddleAnimator.SetBool("isGoingRight", true);

        } else if (Input.GetKeyUp(rightButton))
        {
            //left arm animation is false and sets to idle
            paddleAnimator.SetBool("isGoingRight", false);
        }
    }
}
