using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class leftController : MonoBehaviour
{
    private Animator paddleAnimator;
    private bool isGoingLeft;

    //assign right controller button
    public KeyCode leftButton;

    // Start is called before the first frame update
    void Start()
    {
        //get the animator component and add it to the paddle animation
        paddleAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        //if key code is pressed down
        if (Input.GetKeyDown(leftButton))
        {
            //left arm animation is true
            paddleAnimator.SetBool("isGoingLeft", true);

        } else if (Input.GetKeyUp(leftButton))
        {
            //left arm animation is false and sets to idle
            paddleAnimator.SetBool("isGoingLeft", false);
        }
    }
}