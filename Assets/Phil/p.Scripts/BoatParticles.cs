using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatParticles : MonoBehaviour
{

    public ParticleSystem particle;
    public Rigidbody rb;
    public float show = 0f;


    private float speed = 0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        speed = rb.velocity.magnitude;
        show = speed;
    }
}
