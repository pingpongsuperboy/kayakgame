using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddForce : MonoBehaviour
{

    Rigidbody m_Rigidbody;
    public float m_Thrust = 20f;
    public float m_BackThrust= 0f;

    //declare rowing animator
    public Animator playerAnim;

    //declare the clip name
    string clipName;

    //add two controller buttons
    public KeyCode rightButton;
    public KeyCode leftButton;
    public KeyCode spaceBar;

    public float isGroundedMaxDistance = 1;
    public LayerMask groundedMask;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();

        //get current clip
        var currentClip = this.playerAnim.GetCurrentAnimatorClipInfo(0);
        //get clip name
        clipName = currentClip[0].clip.name;
    }


    void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawRay(transform.position, Vector3.down * isGroundedMaxDistance);
    }

    // Update is called once per frame
    void Update()
    {
        
        // if we're not grounded, don't do anything
        if(!IsGrounded())
            return;

        // otherwise, check if player rowed
        if (Input.GetKeyUp(rightButton) || Input.GetKeyUp(leftButton))
        {
            //Apply a force to this Rigidbody in direction of this GameObjects up axis
            m_Rigidbody.AddForce(transform.forward * m_Thrust);
        } 
        else if (Input.GetKeyDown(spaceBar))
        {
            m_Rigidbody.AddForce(-transform.forward * m_BackThrust);
        }
    }


    bool IsGrounded()
    {
        RaycastHit hit;
        if(Physics.Raycast(transform.position, Vector3.down, out hit, isGroundedMaxDistance, groundedMask)) {
            if(hit.transform.tag == "ground")
                return true;
        }
        

        return false;
    }
}
