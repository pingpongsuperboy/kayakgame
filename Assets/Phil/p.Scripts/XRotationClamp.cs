using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class XRotationClamp : MonoBehaviour
{

    public float speed;
    public Vector3 targetAngle = new Vector3(0f, 0f, 0f);
    private Vector3 currentAngle;

    // Start is called before the first frame update
    void Start()
    {
        currentAngle = transform.eulerAngles;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        currentAngle = new Vector3(
            Mathf.LerpAngle(currentAngle.x, targetAngle.x, Time.deltaTime * speed),
            Mathf.Clamp(currentAngle.y, -25, 25),
            Mathf.LerpAngle(currentAngle.z, targetAngle.z, Time.deltaTime* speed));

            transform.eulerAngles = currentAngle;
    }
}
