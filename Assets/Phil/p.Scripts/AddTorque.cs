using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddTorque : MonoBehaviour
{
    Rigidbody m_Rigidbody;

    public float force;

    //add two controller buttons
    public KeyCode rightButton;
    public KeyCode leftButton;

    // Start is called before the first frame update
    void Start()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
    }



    // Update is called once per frame
    void FixedUpdate()
    {

        if (Input.GetKey(rightButton))
        {
            float turn = Input.GetAxis("Horizontal");
            m_Rigidbody.AddForce(transform.right);
            // Vector3 currentRotation = transform.eulerAngles;
            // currentRotation.y += force * Time.deltaTime;
            // Quaternion newRotation = Quaternion.Euler(currentRotation);
            // transform.rotation = newRotation;

            transform.RotateAroundLocal(Vector3.up, force * Time.deltaTime);
            // m_Rigidbody.AddRelativeTorque(Vector3.up * force);
        }

        if (Input.GetKey(leftButton))
        {
            float turn = Input.GetAxis("Horizontal");
            m_Rigidbody.AddForce(-transform.right);
            // Vector3 currentRotation = transform.eulerAngles;
            // currentRotation.y -= force * Time.deltaTime;
            // Quaternion newRotation = Quaternion.Euler(currentRotation);
            // transform.rotation = newRotation;
            transform.RotateAroundLocal(Vector3.up, -force * Time.deltaTime);
            // m_Rigidbody.AddRelativeTorque(Vector3.up * -force);
        }
    }
}