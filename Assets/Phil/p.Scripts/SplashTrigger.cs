using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SplashTrigger : MonoBehaviour
{
    public ParticleSystem myPart;

    private void OnTriggerEnter(Collider other)
    {
        myPart.Play();
        Debug.Log("Touch");
    }
}