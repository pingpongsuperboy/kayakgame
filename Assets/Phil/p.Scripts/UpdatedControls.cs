using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UpdatedControls : MonoBehaviour
{
    //declare rowing animator
    private Animator playerAnim;

    //declare the clip name
    string clipName;

    //add two controller buttons
    public KeyCode rightButton;
    public KeyCode leftButton;

    //add paddle game objects
    public GameObject rightPaddle;
    public GameObject leftPaddle;

    //the two animation states
    public AnimationClip goLeft;
    public AnimationClip goRight;


    //inverts which button does what
    public bool invertPaddleControls = false;

    // Start is called before the first frame update
    void Start()
    {
        //get the animator component and add it to the paddle animation
        playerAnim = GetComponent<Animator>();

        if(invertPaddleControls) {
            var temp = rightButton;
            rightButton = leftButton;
            leftButton = temp;
        }

    }

    // Update is called once per frame
    void Update()
    {
        //get current clip
        var currentClip = this.playerAnim.GetCurrentAnimatorClipInfo(0);
        //get clip name
        clipName = currentClip[0].clip.name;

        GoLeft();
        GoRight();
        TurnOffPaddles();

    }

    void GoRight()
    {
        //if key code is pressed down the paddle moves right
        if (Input.GetKeyDown(rightButton))
        {
            playerAnim.SetBool("goLeftPush", false);
            playerAnim.SetBool("goRightPull", false);
            playerAnim.SetBool("goRightPush", true);
        } 
        else if (Input.GetKeyUp(rightButton) || clipName == "GoRightPull")
        {
            playerAnim.SetBool("goRightPush", false);
            playerAnim.SetBool("goRightPull", true);
        }
        else
        {
            playerAnim.SetBool("goRightPull", false);
        }
    }

    void GoLeft()
    {
        //if key code is pressed down the paddle moves left
        if (Input.GetKeyDown(leftButton))
        {
            playerAnim.SetBool("goRightPush", false);
            playerAnim.SetBool("goLeftPull", false);
            playerAnim.SetBool("goLeftPush", true);
        } 
        else if (Input.GetKeyUp(leftButton) || clipName == "GoLeftPull")
        {
            playerAnim.SetBool("goLeftPush", false);
            playerAnim.SetBool("goLeftPull", true);
        }
        else
        {
            playerAnim.SetBool("goLeftPull", false);
        }
    }

    void TurnOffPaddles()
    {
        //if going right turn off left paddle
        if (clipName == "GoRightPush" || clipName == "GoRightPull")
        {
            leftPaddle.SetActive(false);
        } else {
            leftPaddle.SetActive(true);
        }
        //if going left turn off right paddle
        if (clipName == "GoLeftPush" || clipName == "GoLeftPull")
        {
            rightPaddle.SetActive(false);
        } else {
            rightPaddle.SetActive(true);
        }
    }
}


  