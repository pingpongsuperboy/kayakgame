using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaveManager : MonoBehaviour
{
    public static WaveManager instance;

    public float amplitude = 1f;
    public float length = 2f;
    public float speed = 1f;
    public float offset = 0f;

    //basic singleton, allows access to a specific instance of this class while also making sure there's only
    //one instance in existance
    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
        else if(instance != this)
        {
            print("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    //incriment the offset by the speed * time.detlatime
    private void Update()
    {
        offset += Time.deltaTime * speed;
    }

    //return the wave height at a given x cord. Sin waves are okay but i'd be interested in using 
    //Gerstner waves if possible
    public float GetWaveHeight(float xCord)
    {
        return amplitude * Mathf.Sin(xCord / length + offset);
    }
}
