using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    public Transform target; //to set the target for the camera
    public Vector3 offset; //to set camera x,y,z offset from the target
                           //otherwise the camera would be inside the target

    //LateUpdate is the exact same as Update, it just runs immediatly
    //after Update does, to avoid issues where Update is doing too many
    //things at the same time, like the target and the camera
    void LateUpdate()
    {
        //camera's current position is equal to the target position + the offset
        transform.position = target.position + offset;

        //keeps the camera facing the target
        transform.LookAt(target);
    }
}
