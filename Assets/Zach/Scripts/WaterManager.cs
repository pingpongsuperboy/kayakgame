using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//enforce mesh filter and renderer components to whatever this is attached to
[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]

public class WaterManager : MonoBehaviour
{
    private MeshFilter meshFilter;

    private void Awake()
    {
        meshFilter = GetComponent<MeshFilter>();
    }

    //grab the mesh's verticies, loop through the array and modify the y components based on the global x
    //components, and reassign the verticies to the mesh and call the recalculate normal method to make sure
    //the normals are correct
    private void Update()
    {
        Vector3[] verticies = meshFilter.mesh.vertices;
        for(int i = 0; i < verticies.Length; i++)
        {
            verticies[i].y = WaveManager.instance.GetWaveHeight(transform.position.x + verticies[i].x);
        }

        meshFilter.mesh.vertices = verticies;
        meshFilter.mesh.RecalculateNormals();
    }
}
