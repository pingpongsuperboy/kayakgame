using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetRotation : MonoBehaviour
{
    Rigidbody rb;

    //Vector3 startPosition;
    //Vector3 endPosition;
    //float smooth = 50f;
    //float tiltAngle = 60f;

    public float percent;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    void Update()
    {
        //startPosition = new Vector3 (gameObject.transform.rotation.x, gameObject.transform.rotation.y, gameObject.transform.rotation.z);
        //endPosition = new Vector3 (0,0,0);
        // Smoothly tilts a transform towards a target rotation.

        if(Input.GetKeyDown(KeyCode.Y)) 
        {
            //float tiltAroundZ = Input.GetAxis("Horizontal") * tiltAngle;
            //float tiltAroundX = Input.GetAxis("Vertical") * tiltAngle;

            // Rotate the cube by converting the angles into a quaternion.
            //Quaternion target = Quaternion.Euler(tiltAroundX, 0, tiltAroundZ);

            // Dampen towards the target rotation
            //transform.rotation = Quaternion.Slerp(transform.rotation, target,  Time.deltaTime * smooth);

            transform.position = new Vector3(transform.position.x, transform.position.y + 1, transform.position.z);
            transform.rotation = Quaternion.Euler(0, transform.rotation.eulerAngles.y, 0);

            //transform.position = Vector3.Slerp(startPosition, endPosition, percent);
        }
    }
}
