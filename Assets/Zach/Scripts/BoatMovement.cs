using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoatMovement : MonoBehaviour
{
    public float speed = 2f; //declare editable variable speed
    private Rigidbody rb; //declare the rigidbody as rb

    //called before the first frame update
    void Start()
    {
        //gets the rigidbody for the ball
        rb = GetComponent<Rigidbody>();
    }

    //unlike Update, FixedUpdate runs at the set framerate of 
    //the project for better physics calculations
    void FixedUpdate()
    {
        //gets the value of the horizontal and vertical input axes as a float
        //unlike get button which returns a bool, get axis returns a float from
        //the inputs set in project settings (WASD)
        float movementHorizontal = Input.GetAxis("Horizontal");
        float movementVertical = Input.GetAxis("Vertical");

        //uses the input axes floats in a new vector3
        Vector3 movement = new Vector3 (movementHorizontal, 0f, movementVertical);

        //uses built-in method to add force to the rigidbody, using the 
        //vector3 of movement times the speed
        rb.AddForce(movement * speed);
    }
}
