using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Floater : MonoBehaviour
{
    public Rigidbody rigidBody; //reference the rigidbody
    public float depthBeforeSubmerged = 1f;
    public float displacementAmount = 3f;
    public int floaterCount =1;

    public float waterDrag = 0.99f;
    public float waterAngularDrag = 0.5f;

    private void FixedUpdate()
    {
        rigidBody.AddForceAtPosition(Physics.gravity / floaterCount, transform.position, ForceMode.Acceleration);
        
        //gets wave height at the floater's x pos
        float waveHeight = WaveManager.instance.GetWaveHeight(transform.position.x);

        if(transform.position.y < waveHeight) //check if the y pos is less than wave height and apply buoyancy force
        {
            //calcuate the displacement multiplyer by subtracting the wave height from the floater's y pos, inverting it
            //it to make it positive, dividing it by the depth before submerged value, and clamping between 0 and 1,
            //affecting how strong the bouyancy force will be. clamp between 0 and 1 because once an object is fully submerged,
            //the buoyancy force remains the same
            float displacementMultiplier = Mathf.Clamp01((waveHeight - transform.position.y) / depthBeforeSubmerged) * displacementAmount;

            //adds upwards force to the floater position with a y componant equal to the force of gravity multiplied by
            //the displacement multiplier, using the acceleration force mode because the buoyancy force shouldn't
            //be affected by the object's mass
            rigidBody.AddForceAtPosition(new Vector3(0f, Mathf.Abs(Physics.gravity.y) * displacementMultiplier, 0f), transform.position, ForceMode.Acceleration);

            //adds drag to the floaters for fine-tuning. angular velocity needs the add torque since it's dealing with rotation
            rigidBody.AddForce(displacementMultiplier * -rigidBody.velocity * waterDrag * Time.fixedDeltaTime, ForceMode.VelocityChange);
            rigidBody.AddTorque(displacementMultiplier * -rigidBody.angularVelocity * waterAngularDrag * Time.fixedDeltaTime, ForceMode.VelocityChange);
        }
    }
}
