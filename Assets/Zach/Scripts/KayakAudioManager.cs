using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KayakAudioManager : MonoBehaviour
{
    public GameObject player;
    public AudioSource kayakSlide;

    //called before the first frame update
    void Start()
    {
        kayakSlide = GetComponent<AudioSource>();
    }

    //called once per frame
    void Update()
    {
        //var wasGrounded = false;
 
        //if(player.isGrounded && !wasGrounded) // just hit the ground
        kayakSlide.Play();

        //else if (wasGrounded && !player.isGrounded) // just left the ground
        //kayakSlide.Pause();

        //wasGrounded = player.isGrounded;
    }
}
