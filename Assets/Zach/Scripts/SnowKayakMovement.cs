using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowKayakMovement : MonoBehaviour
{
    public Transform m_kayak;
    public float m_alignSpeed = 5;
    public float m_rayDistance = 5f;
    [Range(-1, 1)]
    public float m_Forward;
    public float m_FwdForce = 10;
    public float m_Side;
    private Vector3 m_surfaceNormal = new Vector3();
    private Vector3 m_collisionPoint = new Vector3();
    public bool m_useRaycast = true;
    private bool m_onSurface;
    private Collision m_surfaceCollisionInfo;
    private Rigidbody m_rigidbody;

    //from AddTorque script
    public float force;
    public KeyCode rightButton;
    public KeyCode leftButton;
    public KeyCode spaceBar;
    public float m_Thrust = 20f;
    public float m_BackThrust = 0f;
    
    
    //called before the first frame update
    void Start()
    {
        m_rigidbody = GetComponent<Rigidbody>();
    }

    //from AddTorque script
    void Update()
    {
        if (Input.GetKey(rightButton))
        {
            float turn = Input.GetAxis("Horizontal");
            m_rigidbody.AddForce(transform.right);
            // Vector3 currentRotation = transform.eulerAngles;
            // currentRotation.y += force * Time.deltaTime;
            // Quaternion newRotation = Quaternion.Euler(currentRotation);
            // transform.rotation = newRotation;

            transform.Rotate(Vector3.up, force * Time.deltaTime);
            // m_Rigidbody.AddRelativeTorque(Vector3.up * force);
        }

        if (Input.GetKey(leftButton))
        {
            float turn = Input.GetAxis("Horizontal");
            m_rigidbody.AddForce(-transform.right);
            // Vector3 currentRotation = transform.eulerAngles;
            // currentRotation.y -= force * Time.deltaTime;
            // Quaternion newRotation = Quaternion.Euler(currentRotation);
            // transform.rotation = newRotation;
            transform.Rotate(Vector3.up, -force * Time.deltaTime);
            // m_Rigidbody.AddRelativeTorque(Vector3.up * -force);
        }

        // if we're not grounded, don't do anything
        if(!m_onSurface)
            return;

        // otherwise, check if player rowed
        if (Input.GetKeyUp(rightButton) || Input.GetKeyUp(leftButton))
        {
            //Apply a force to this Rigidbody in direction of this GameObjects up axis
            m_rigidbody.AddForce(transform.forward * m_Thrust);
        } 
        else if (Input.GetKeyDown(spaceBar))
        {
            m_rigidbody.AddForce(-transform.forward * m_BackThrust);
        }

    }

    // // Update is called once per frame
    // void Update()
    // {
    //     ProcessInputs();
    // }

    // private void FixedUpdate()
    // {
    //     AlignToSurface();
    //     ProcessForce();
    // }

    // private void ProcessInputs()
    // {
    //     m_Forward = Input.GetAxis("Vertical");
    //     m_Side = Input.GetAxis("Horizontal");

    //     Vector3 movement = new Vector3 (m_Side, 0f, m_Forward);
    // }

    // private void ProcessForce()
    // {
    //     if (!m_onSurface)
    //         return;

    //     //m_rigidbody.AddForce(m_kayak.forward * m_FwdForce * m_Forward);
    //     Vector3 movement = new Vector3 (m_Side, 0f, m_Forward);
    //     m_rigidbody.AddForce(movement * m_FwdForce);
    // }

    private void OnCollisionStay(Collision other)
    {
        m_onSurface = true;
        m_surfaceCollisionInfo = other;
        m_surfaceNormal = other.GetContact(0).normal;
        m_collisionPoint = other.GetContact(0).point;
    }

    private void OnCollisionExit(Collision other)
    {
        m_surfaceCollisionInfo = null;
        m_onSurface = false;
    }

    // void AlignToSurface()
    // {
    //     if (m_useRaycast)
    //     {
    //         var hit = new RaycastHit();
    //         var onSurface = Physics.Raycast(transform.position, Vector3.down, out hit, m_rayDistance);
    //         if (onSurface)
    //         {
    //             var localRot = Quaternion.FromToRotation(transform.up, hit.normal) * transform.rotation;
    //             var euler = localRot.eulerAngles;
    //             euler.y = 0;
    //             localRot.eulerAngles = euler;
    //             m_kayak.localRotation = Quaternion.LerpUnclamped(m_kayak.localRotation, localRot, m_alignSpeed * Time.fixedDeltaTime);
    //         }
    //     }
    //     else
    //     {
    //         if (m_onSurface)
    //         {
    //             var localRot = Quaternion.FromToRotation(transform.up, m_surfaceNormal) * transform.rotation;
    //             var euler = localRot.eulerAngles;
    //             euler.y = 0;
    //             localRot.eulerAngles = euler;
    //             m_kayak.localRotation = Quaternion.LerpUnclamped(m_kayak.localRotation, localRot, m_alignSpeed * Time.fixedDeltaTime);
    //         }
    //     }
    // }
}
